-- Should welcome new player with a great popup

minetest.register_on_joinplayer(function(player)
      local name = player:get_player_name()
      local welcome_formspec = "size[8,5.2]"..
	 "label[1.0,0.3;Bienvenue "..name..
	 " sur le serveur PVP/Faction de Solebull !\n\n"..
	 "Ce serveur et cette map ne servent pour le moment qu'a des fins\n"..
	 "de test et de développement. \n\n"..
	 "La configuration peut changer a tout moment et la map peut être\n"..
	 "réinitialisée au prochain stream. \n\nMerci et bon jeu!]"..
	 "button_exit[3.0,4.5;2,1;exit;OK]"
      
      minetest.show_formspec(name, "default:welcome_popup", welcome_formspec)
end)
