--Fake Nodes--

minetest.register_craft({
	output = 'camo:fake_dirt 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:dirt', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_dirt_with_grass 4',
	recipe = {
		{'default:leaves', 'default:grass_1', 'default:leaves'},
		{'default:leaves', 'default:dirt', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_stone 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:stone', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_cobble 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:cobble', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_sand 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:sand', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_sandstone 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:sandstone', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_desert_sand 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:desert_sand', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_desert_stone 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:desert_stone', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_gravel 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:gravel', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_bookshelf 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:bookshelf', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_wood 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:wood', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_tree 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:tree', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_leaves 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_furnace 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:furnace_inactive', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_chest 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:chest', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_glass 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:glass', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_clay 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:clay', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

minetest.register_craft({
	output = 'camo:fake_brick 4',
	recipe = {
		{'default:leaves', 'default:leaves', 'default:leaves'},
		{'default:leaves', 'default:brick', 'default:leaves'},
		{'default:leaves', 'default:leaves', 'default:leaves'},
	}
})

--Hidden Ladders--

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:dirt_ladder',
    recipe = {'camo:fake_dirt', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:dirt_with_grass_ladder',
    recipe = {'camo:fake_dirt_with_grass', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:stone_ladder',
    recipe = {'camo:fake_stone', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:cobble_ladder',
    recipe = {'camo:fake_cobble', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:sand_ladder',
    recipe = {'camo:fake_sand', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:sandstone_ladder',
    recipe = {'camo:fake_sandstone', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:desert_sand_ladder',
    recipe = {'camo:fake_desert_sand', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:desert_stone_ladder',
    recipe = {'camo:fake_desert_stone', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:gravel_ladder',
    recipe = {'camo:fake_gravel', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:bookshelf_ladder',
    recipe = {'camo:fake_bookshelf', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:wood_ladder',
    recipe = {'camo:fake_wood', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:tree_ladder',
    recipe = {'camo:fake_tree', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:leaves_ladder',
    recipe = {'camo:fake_leaves', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:glass_ladder',
    recipe = {'camo:fake_glass', 'default:ladder'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:clay_ladder',
    recipe = {'camo:fake_clay', 'default:ladder'},
})

--One Way Glass--

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:dirt_glass',
    recipe = {'camo:fake_dirt', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:dirt_grass_glass',
    recipe = {'camo:fake_dirt_with_grass', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:stone_glass',
    recipe = {'camo:stone_dirt', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:cobble_glass',
    recipe = {'camo:fake_cobble', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:sand_glass',
    recipe = {'camo:fake_sand', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:sandstone_glass',
    recipe = {'camo:fake_sandstone', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:desert_sand_glass',
    recipe = {'camo:fake_desert_sand', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:desert_stone_glass',
    recipe = {'camo:fake_desert_stone', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:gravel_glass',
    recipe = {'camo:fake_gravel', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:bookshelf_glass',
    recipe = {'camo:fake_bookshelf', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:wood_glass',
    recipe = {'camo:fake_wood', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:tree_glass',
    recipe = {'camo:fake_tree', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:furnace_glass',
    recipe = {'camo:fake_furnace', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:chest_glass',
    recipe = {'camo:fake_chest', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:clay_glass',
    recipe = {'camo:fake_clay', 'default:glass'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:brick_glass',
    recipe = {'camo:fake_brick', 'default:glass'},
})




--Traps--

minetest.register_craft({
	output = 'camo:small_spike 4',
	recipe = {
		{'', 'default:steel_ingot', ''},
		{'', 'default:steel_ingot', ''},
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
	}
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:large_spike',
    recipe = {'camo:small_spike', 'camo:small_spike', 'camo:small_spike', 'camo:small_spike'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:spike_grass',
    recipe = {'camo:large_spike', 'default:grass_1'},
})

minetest.register_craft({
	type = 'shapeless',
    output = 'camo:catapulter',
    recipe = {'camo:fake_dirt_with_grass', 'wool:white'},
})

--ALARMS--

--Manual Alarm--
minetest.register_craft({
	output = 'camo:manual_alarm',
	recipe = {
		{'', 'default:mese_crystal_fragment', ''},
		{'', 'camo:urban_camo', ''},
		{'', '', ''},
	}
})

--Intruder Alarm--
minetest.register_craft({
	output = 'camo:intruder_alarm',
	recipe = {
		{'', 'default:mese_crystal_fragment', ''},
		{'', 'camo:manual_alarm', ''},
		{'', '', ''},
	}
})



