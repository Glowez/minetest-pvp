--[[
death_messages - A Minetest mod which sends a chat message when a player dies.
Copyright (C) 2016  EvergreenTree

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-----------------------------------------------------------------------------------------------
local title = "Death Messages"
local version = "0.1.2"
local mname = "death_messages"
-----------------------------------------------------------------------------------------------
dofile(minetest.get_modpath("death_messages").."/settings.txt")
-----------------------------------------------------------------------------------------------

-- A table of quips for death messages.  The first item in each sub table is the
-- default message used when RANDOM_MESSAGES is disabled.
local messages = {}

-- Lava death messages
messages.lava = {
	" a rencontré une boule de feu.",
	" a pensé que la lave est cool.",
	" ne pouvait pas résister à la lave.",
	" a creusé tout droit.",
	" ne savait pas que la lave est chaude."
}

-- Drowning death messages
messages.water = {
	" s'est noyé.",
	" a manqué d'air.",
	" a échoué aux cours de natation.",
	" a essayé de se faire passer pour une ancre.",
	" a oublié qu'il n'était pas un poisson.",
	" a fait trop de bulles.",
	" a passé trop de temps dans un lavabo."
}

-- Burning death messages
messages.fire = {
	" a brûlé à souhait.",
	" a eu un peu trop chaud.",
	" s'est approché trop près du feu de camp.",
	" vient d'être rôti, style hot-dog.",
	" s'est brûlé."
}

-- Other death messages
messages.other = {
	" est mort.",
	" a fait quelque chose de fatal.",
	" a abandonné la vie.",
	" est un peu mort maintenant.",
	" s'est évanoui de façon permanente.",
	" est mort d'impatience.",
	" a peut-être rencontré Séréna."
}

function get_message(mtype)
	if RANDOM_MESSAGES then
		return messages[mtype][math.random(1, #messages[mtype])]
	else
		return messages[1] -- 1 is the index for the non-random message
	end
end

minetest.register_on_dieplayer(function(player)
	local player_name = player:get_player_name()
	local node = minetest.registered_nodes[minetest.get_node(player:getpos()).name]
	if minetest.is_singleplayer() then
		player_name = "You"
	end
	-- Death by lava
	if node.groups.lava ~= nil then
		minetest.chat_send_all(player_name .. get_message("lava"))
	-- Death by drowning
	elseif player:get_breath() == 0 then
		minetest.chat_send_all(player_name .. get_message("water"))
	-- Death by fire
	elseif node.name == "fire:basic_flame" then
		minetest.chat_send_all(player_name .. get_message("fire"))
	-- Death by something else
	else
		minetest.chat_send_all(player_name .. get_message("other"))
	end

end)

-----------------------------------------------------------------------------------------------
print("[Mod] "..title.." ["..version.."] ["..mname.."] Loaded...")
-----------------------------------------------------------------------------------------------
