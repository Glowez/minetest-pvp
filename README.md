# Solebull's PvP/Faction Minetest game

## En tant que joueur

Ce jeu minetest est basé sur
[Cobalt](https://forum.minetest.net/viewtopic.php?f=50&t=16034) 

1. Télecharger  [Minetest](https://www.minetest.net/downloads/).
   De préference la version 0.4.17.1

2. Connectez vous au serveur nommé *Solebull*. Avant de vous connecter
   laissez nous un message contenant votre pseudo dans le chat : ce serveur
   utilise le mod *whitelist*.

## En tant sue développeur

Si vous souhaitez contribuer au développement du jeu, il vous faut utiliser
un client `git` pour cloner le dépôt :

Allez dans le répertoire minetest/games/ puis :

	git clone https://gitlab.com/solebull/minetest-pvp.git

## En tant qu'administrateur

Après la géneration de la map, il faut y définir une *safe zone PVP* autout du
point de spawn. Le reste de la carte sera PVP par défaut.

Il faut définir le centre du spawn (pos1) et le point de la zone le plus éloigné
(pos2) :

	/pvp_areas pos1
	/pvp_areas pos2
	/pvp_areas set


## Installation sous Debian GNU/Linux Stretch

La version 0.4.16 n'est pas dans les dépôts Debian Stretch. Il faut
donc le compiler manuellement :

	sudo apt install build-essential libirrlicht-dev cmake libbz2-dev \
	  libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev libvorbis-dev     \
	  libluajit-5.1-dev
	git clone https://github.com/minetest/minetest.git
	cd minetest
	git checkout 0.4.16
	cd build
	cmake ..
	make
	sudo checkinstall --pkgname=minetest --fstrans=no --backup=no     \
	  --pkgversion="0.4.16" --deldoc=yes

A partir de Debian Buster, la version des dépôts officielle fonctionne
parfaitement.

Ensuite, cloner et instaler minetest-pvp dans le répertoire
*~/.minetest/games*. Ce repertoire peut ne pas exister si vous n'avez pas 
encore lancé *minetest*. Pour le créer lancez la comande suivante :

	mkdir -p ~/.minetest/games
	cd ~/.minetest/games
	git clone git@gitlab.com:solebull/minetest-pvp.git


# Recettes
## Cisailles (Steel Shears)

Il s'agit des cisailles, utilisées pour récupérer la laine des moutons


- steel:none
- stick:steel

## Envoyer vers l'inventaire/stacker

Lorsqu'on click sur <kbd>Shift</kbd> + <kbd>click</kbd>, on envoi rapidement
vers l'inventaire en stackant autant que possible.

# Mapper

Le *mapper* utilisé côté serveur est 
[minetestmapper](https://github.com/minetest/minetestmapper).

## Compilation

	sudo apt install cmake libgd-dev checkinstall
	git clone https://github.com/minetest/minetestmapper.git
	cd minetestmapper/
	mkdir build
	cd build/
	cmake ..
	make
	sudo checkinstall --pkgname minetestmapper

## Utilisation

	minetestmapper -i ~/.minetest/worlds/MyWorld/ -o map.png

# Troubleshooting
## FPS très bas

Si les FPS rendent le jeu dificilement jouable (10-15 FPS) :

1. Aller dans *settings*;
2. Désactiver tous les *shaders*;
3. **Redémarrer le jeu**.

# Factions

La gestion des factions est assutrée par 
[factions](https://github.com/agrecascino/factions). 
Beaucoup de questions restent sans réponses concernant ce mod.

## Parcelles

La protection fonctionne par parcelles. Chaque faction peut prétender (*claim*)
une parcelle suivant sa puissance.

## Faction chest

Les coffres de guild (*faction chest*) ne peuvent être ouvert si ils sont dans 
une parcelle appartenant a la guilde

## Commandes

Voici la liste des commandes implémentées. Pour y accéder taper 
`/factions <cmd>` :

    claim : Claim the plot of land you're on
    unclaim : Unclaim the plot of land you're on.
    list : List all registered factions
    version : Displays mod version.
    info : Shows a faction's description
    leave : Leave your faction
    kick: Kick a player from your faction
    create : Create a new faction
    join : Join a faction
    disband : Disband your faction
    close : Make your faction invite-only
    open : Allow any player to join your faction
    description : Set your faction's description
    invite : Invite a player to your faction
    uninvite : Revoke a player's invite
    delete : Delete a faction
    ranks : List ranks within your faction (eader, member, moderator)
    who : List players in your faction, and their ranks.
    newrank : Add a new rank
    delrank : Replace and delete a rank
    setspawn : Set the faction's spawn
    where : See whose parcel you stand on
    help : Shows help for commands
    spawn : Shows your faction's spawn
    promote : Promotes a player to a rank
    power : Display your faction's power
    setbanner : Sets the banner you're on as the faction's banner
    convert : Load factions in the old format
    free : Forcefully frees a parcel
    chat : Send a message to your faction's members
    forceupdate : Forces an update tick
    which : Gets a player's faction
    setleader : Set a player as a faction's leader
    setadmin : Make a faction an admin faction
    resetpower : Reset a faction's power
    obliterate : Remove all factions
    getspawn : Get a faction's spawn
    whoin : Get all members of a faction
    stats : Get stats of a faction
    seen : Check the last time a faction had a member logged in
